Introduction
------------

Thank you for considering contributing to this project. Any contribution
is extremely welcome and appreciated.

These guidelines are designed to help all contributors understand how to
work and interact within this project.

Basics
------

* Bug reports and merge requests via GitLab are very welcome
* Contributions include testing, writing more tests, documentation,
  submitting bug reports, and proposing new features and tests
* Issues and changes should be small and focused on a particular topic
* Contributors are expected to abide by the
  [code of conduct](CODE_OF_CONDUCT.md)

Community
---------

All project interaction takes place on GitLab. The project community is
essentially the author at the moment. However, this project exists within
the greater GNU Octave community. It is expected, and the author's hope,
that at some point in the future, this project will be merged into GNU
Octave and cease to exist as an independent project.
