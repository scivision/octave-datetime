## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} years (@var{x})
## @seealso{days, duration, hours, milliseconds, minutes, seconds}
## @end deftypefn

function y = years (x)

  if (nargin != 1)
    print_usage ();
  endif

  if (! isnumeric (x))
    error ("years: X must be a numeric array or a duration array");
  endif

  if (isnumeric (x))
    y = duration (x * (365.2425 * 24), 0, 0, "Format", "y");
  endif

endfunction

%!assert (isobject (years (0)))
%!assert (isa (years (0), "duration"))
%!assert (size (years (0)), [1, 1])
%!assert (years (years (1:10)), 1:10)

## Test input validation
%!error years ()
%!error years (1, 2)
%!error years ("s")
