## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} duration ()
## @end deftypefn

classdef duration

  properties
    Format;
  endproperties

  properties (Access = "private")
    m_milliseconds;
  endproperties

  methods

    function self = duration (varargin)

      if (nargin < 1)
        error ("duration: must be called with H, M, S arguments");
      endif

      nextarg = 1;

      if (isnumeric (varargin{1}))
        if ((nargin >= 3) && isnumeric (varargin{2}) && isnumeric (varargin{3}))
          p_hours = varargin{1};
          p_minutes = varargin{2};
          p_seconds = varargin{3};
          if ((nargin >= 4) && isnumeric (varargin{4}))
            p_milliseconds = varargin{4};
            nextarg = 5;
          else
            p_milliseconds = 0;
            nextarg = 4;
          endif
        elseif (columns (varargin{1}) == 3)
          p_hours = varargin{1}(:,1);
          p_minutes = varargin{1}(:,2);
          p_seconds = varargin{1}(:,3);
          p_milliseconds = 0;
          nextarg = 2;
        else
          error ("duration: must be called with H, M, S arguments");
        endif

        self.m_milliseconds = p_hours * (60 * 60 * 1000) ...
                            + p_minutes * (60 * 1000) ...
                            + p_seconds * 1000 ...
                            + p_milliseconds;
      endif

      fmt = "hh:mm:ss";

      while (nextarg < nargin)
        if (strcmpi (varargin{nextarg}, "Format"))
          s = varargin{nextarg+1};
          switch (s)
            case {"y", "d", "h", "m", "s", "hh:mm"}
              fmt = s;
            otherwise
              if (regexp (s, '^dd:hh:mm:ss(\.S{1,9})?$'))
                fmt = s;
              elseif (regexp (s, '^hh:mm:ss(\.S{1,9})?$'))
                fmt = s;
              elseif (regexp (s, '^mm:ss(\.S{1,9})?$'))
                fmt = s;
              else
                error ("duration: Format must be a recognized pattern");
              endif
          endswitch
          nextarg += 2;
        elseif (strcmpi (varargin{nextarg}, "InputFormat"))
          nextarg += 2;
        else
          error ("duration: unexpected arguments");
        endif
      endwhile

      self.Format = fmt;

    endfunction

    function varargout = size (self, varargin)
      [varargout{1:nargout}] = size (self.m_milliseconds, varargin{:});
    endfunction

    function c = columns (self)
      c = size (self.m_milliseconds, 2);
    endfunction

    function r = rows (self)
      r = size (self.m_milliseconds, 1);
    endfunction

    function varargout = subsref (self, idx)
      switch (idx(1).type)
        case "."
          [varargout{1:nargout}] = builtin ("subsref", self, idx);
        case "()"
          if (numel (idx) == 1)
            ms = builtin ("subsref", self.m_milliseconds, idx);
            varargout{1} = duration (0, 0, ms / 1000);
          elseif ((numel (idx) == 2) && strcmp (idx(2).type, "."))
            value = builtin ("subsref", self, idx(2));
            if (strcmp (idx(1).subs, ":"))
              n = numel (self.m_milliseconds);
            else
              n = numel (idx(1).subs);
            endif
            [out{1:n}] = deal (value);
            varargout = out;
          else
            [varargout{1:nargout}] = builtin ("subsref", self, idx);
          endif
        case "{}"
          [varargout{1:nargout}] = builtin ("subsref", self, idx);
        otherwise
          error ("duration.subsref: invalid indexing expression");
      endswitch
    endfunction

    function y = days (self)
      y = self.m_milliseconds / (1000 * 60 * 60 * 24);
    endfunction

    function y = hours (self)
      y = self.m_milliseconds / (1000 * 60 * 60);
    endfunction

    function y = milliseconds (self)
      y = self.m_milliseconds;
    endfunction

    function y = minutes (self)
      y = self.m_milliseconds / (1000 * 60);
    endfunction

    function y = seconds (self)
      y = self.m_milliseconds / 1000;
    endfunction

    function y = years (self)
      y = self.m_milliseconds / (1000 * 60 * 60 * 24 * 365.2425);
    endfunction

    y = plus (self, varargin);
    y = minus (self, other);

    y = horzcat (self, varargin);
    y = vertcat (self, varargin);

  endmethods

endclassdef

%!error duration ()
%!error duration (1)
%!error duration (1, 2)
%!error duration (1, 2, 3, 4, 5, 6)
%!error duration ([1, 2])
%!error duration ([1, 2, 3, 4])
%!error duration ([1; 2])
