## Copyright (C) 2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-

function y = horzcat (self, varargin)

  if (! all (cellfun (@isduration, varargin)))
    error ("duration.horzcat: arguments must be duration");
  endif

  ms = self.m_milliseconds;
  other = cellfun (@(v) milliseconds (v), varargin, "uniformoutput", false);
  ms = builtin ("horzcat", ms, other{:});
  y = duration (0, 0, 0, ms, "Format", self.Format);

endfunction
