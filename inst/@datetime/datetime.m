## Copyright (C) 2016-2018 Mike Miller
##
## This file is part of Octave.
##
## Octave is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## Octave is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with Octave; see the file COPYING.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {} datetime ()
## @end deftypefn

classdef datetime

  properties
    Format;
    TimeZone;
    Year;
    Month;
    Day;
    Hour;
    Minute;
    Second;
  endproperties

  methods

    function self = datetime (varargin)

      if (nargin == 0)
        v = clock ();
      elseif (ischar (varargin{1}) || iscellstr (varargin{1}))
        v = datevec (varargin{1});
      elseif (nargin == 3 || nargin == 6) && all(cellfun(@isscalar, varargin))
        v = cell2mat(varargin);
      else
        v = varargin{1};
      endif

      self.Year =   v(:,1);
      self.Month =  v(:,2);
      self.Day =    v(:,3);
      if size(v, 2) == 6
        self.Hour =   v(:,4);
        self.Minute = v(:,5);
        self.Second = v(:,6);
      else
        self.Hour = zeros(size(self.Year));
        self.Minute = zeros(size(self.Year));
        self.Second = zeros(size(self.Year));
      end
      self.Format = "default";
      self.TimeZone = "";

    endfunction

  endmethods

endclassdef
